# Docker-XAMPP

Run bash in container
````
(alias docker="winpty docker") for git-bash
docker exec -it [container-id] bash
````

Rebuild service
````
docker-compose up -d --no-deps --build nginx
````

Execute shell in container
````
docker ps
docker exec -it <container name> sh
````

load filebeat-dashboards
````
docker run --net="host" docker.elastic.co/beats/filebeat:7.16.0 setup --dashboards
````

load metricbeat-dashboards
````
docker run --net="host" docker.elastic.co/beats/metricbeat:7.16.0 setup --dashboards
````

load packetbeat-dashboards
````
docker run --cap-add=NET_ADMIN --net="host" docker.elastic.co/beats/packetbeat:7.16.0 setup --dashboards
````